	.file	"matrix64.c"
	.text
	.p2align 4,,15
	.globl	array_int
	.type	array_int, @function
array_int:
.LFB7:
	.cfi_startproc
	xorl	%eax, %eax
	cmpq	$1, %rsi
	jbe	.L2
	addq	$1, %rsi
	movl	$2, %ecx
	movl	$1, %edx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r8, %rcx
.L3:
	leaq	1(%rcx), %r8
	addl	(%rdi,%rdx,4), %eax
	subl	-4(%rdi,%rdx,4), %eax
	movq	%rcx, %rdx
	cmpq	%rsi, %r8
	jne	.L5
.L2:
	rep
	ret
	.cfi_endproc
.LFE7:
	.size	array_int, .-array_int
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB8:
	.cfi_startproc
	subq	$79880, %rsp
	.cfi_def_cfa_offset 79888
	movl	$1, %eax
	leaq	-120(%rsp), %rcx
	leaq	79880(%rsp), %rsi
	.p2align 4,,10
	.p2align 3
.L8:
	xorq	%rdx, %rdx
	.p2align 4,,10
	.p2align 3
.L9:
	addq	(%rcx,%rdx,8), %rax
	addq	$1, %rdx
	cmpq	$100, %rdx
	jne	.L9
	addq	$800, %rcx
	cmpq	%rsi, %rcx
	jne	.L8
	addq	$79880, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE8:
	.size	main, .-main
	.ident	"GCC: (Debian 4.6.0-10) 4.6.1 20110526 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
