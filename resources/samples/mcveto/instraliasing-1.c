//@summarystart
// Example which illustrates McVeto's ability to deal with
// instruction aliasing. 
//@summaryend
// %keyword cav10-2

#include "../include/reach.h"

int n;

int foo(int a) { // In the object file, the address of foo is 0
  n += a;
  return 1;
}

int main() {

  int x;
  int y;

  n = y;

  if(y<0)
      n=3;
  else
      n=4;

  __asm {
      mov eax, 2;
  L1: mov edx, 0xd0ff046a; // This instruction becomes push 4; call eax; when it's read at the third byte
      cmp eax, 1;
      jz  L2;
      mov eax, foo;
      lea ebx, L1+1;
      jmp ebx;
  L2: xor edx, edx;
  }
#if REACH==0
  if(n == 2)
    UNREACHABLE();
#else 
  if(n == 7)
    REACHABLE(); // Reachable: 3 + 4 = 7
#endif
  
  return 0;
}

