
format elf64
	
section '.data' writable

foo		dw 0x2342
msg		db 'Hello World64!', 0x0A
msg_size	= $-msg

section '.text' executable

public main

main:
   mov rax, [eax]
   mov eax, [ebx]
   mov eax, [ebx+edi]
   mov eax, [ebx+edi*4]
   mov eax, [edi*4]
   mov eax, [edi*4+0xff]
   mov eax, [ebx+edi*4+0xff]
   mov eax, [ebx+edi*4+0xffff]
   mov eax, [ebx+edi*4-1]
   mov eax, [ebx+edi*4-255]
   mov eax, [ebx+edi*4-65535]

   mov rax, [rax]
   mov rax, [rbx+rdi]
   mov rax, [rdi*8]
  mov rax, 0xffffffffffffffff

   mov r13, [r14+r15*8-1]
   mov r13, [r14]
   mov r13, [r14+r15*8]
   mov r13, [r15*8]

   ; exit
   xor     edi, edi
   mov     eax, 60
   syscall
