
format elf
	
section '.data' writable

foo		dw 0x2342
msg		db 'Hello World64!', 0x0A
msg_size	= $-msg

section '.text' executable

public main

main:
   mov eax, [ebx]
   mov eax, [ebx+edi]
   mov eax, [ebx+edi*4]
   mov eax, [edi*4]
   mov eax, [edi*4+0xff]
   mov eax, [ebx+edi*4+0xff]
   mov eax, [ebx+edi*4+0xffff]
   mov eax, [ebx+edi*4-1]
   mov eax, [ebx+edi*4-255]
   mov eax, [ebx+edi*4-65535]
   ; exit
   xor     edi, edi
   mov     eax, 60
   syscall
