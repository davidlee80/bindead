package bindead.domains;

import static bindead.TestsHelper.lines;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;

import rreil.lang.RReilAddr;
import bindead.analyses.Analysis;
import bindead.analyses.AnalysisFactory;
import bindead.analyses.algorithms.data.CallString;
import bindead.debug.XmlToHtml;
import bindead.domainnetwork.interfaces.RootDomain;

/**
 * Test environment for the XSLT parser that transforms the XML output
 * of the abstract domains.
 *
 * @author Bogdan Mihaila
 */
public class XmlOutput extends Application {
  Stage stage;

  public static void main (String[] args) {
    launch(); // start the JavaFX Application. Needs to be done for framework init code to run.
  }

  @Override public void start (Stage stage) {
    // create the scene
    stage.setTitle("Web View");
    this.stage = stage;
    output();
  }

  @SuppressWarnings("rawtypes") public void output () {
//    DebugHelper.analysisKnobs.printMemVarOnly(); // print the variables shorter
//    RootDomain state = generateAnalysisOutput1();
//    RootDomain state = generateAnalysisOutput2();
    RootDomain state = generateAnalysisOutput3();
    String xmlOutput = state.toXml();
    String printOutput = state.toString();
//    System.out.println(printOutput);
//    System.out.print(xmlOutput);
    String htmlOutput = XmlToHtml.renderAsHtml(xmlOutput);
//    System.out.println(htmlOutput);
    showFrameWithOutputs(printOutput, xmlOutput, htmlOutput);
  }

  public void showFrameWithOutputs (final String textdump, final String xml, final String html) {
//      SwingUtilities.invokeLater(new Runnable() {
    Platform.runLater(new Runnable() { // this will run initFX as JavaFX-Thread
          @Override public void run () {
            // JavaFX code
            Group group = new Group();
            Scene scene = new Scene(group);
            group.getStyleClass().add("browser");

            JFXPanel fxPanel = new JFXPanel();
            fxPanel.setScene(scene);

            WebView browser = new WebView();
            browser.getEngine().loadContent(html);
            group.getChildren().add(browser);
            browser.setMinSize(1500, 500);

            // Swing code components instantiation code
            JTabbedPane tabbedPane = new JTabbedPane();
            tabbedPane.addTab("HTML(JavaFX)", null, new JScrollPane(fxPanel), "Displays the rendered HTML.");
            tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

            // Old Swing based HTML renderer that does not execute Javascript
            JTextPane htmlDisplay = new JTextPane();
            htmlDisplay.setEditable(false);
            htmlDisplay.setContentType("text/html");
            htmlDisplay.setText(html);
            tabbedPane.addTab("HTML(Swing)", null, new JScrollPane(htmlDisplay), "Displays the rendered HTML.");
            tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

            JTextPane htmlDisplayText = new JTextPane();
            htmlDisplayText.setEditable(false);
            htmlDisplayText.setText(html);
            tabbedPane.addTab("HTML(text)", null, new JScrollPane(htmlDisplayText), "Displays the _not_ rendered HTML.");
            tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);

            JTextPane xmlDisplay = new JTextPane();
            xmlDisplay.setEditable(false);
            xmlDisplay.setText(xml);
            tabbedPane.addTab("XML", null, new JScrollPane(xmlDisplay), "Displays the XML output.");
            tabbedPane.setMnemonicAt(3, KeyEvent.VK_4);

            JTextPane textdumpDisplay = new JTextPane();
            textdumpDisplay.setEditable(false);
            textdumpDisplay.setText(textdump);
            tabbedPane.addTab("Text", null, new JScrollPane(textdumpDisplay), "Displays the text debug dump output.");
            tabbedPane.setMnemonicAt(4, KeyEvent.VK_5);


            JFrame frame = new JFrame("HTML Domain View");
            frame.getContentPane().setLayout(new BorderLayout());
//              frame.getContentPane().setPreferredSize(new Dimension(1200, 800));
            frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
            frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
            frame.addWindowListener(new WindowAdapter() {
              @Override public void windowClosing (WindowEvent e) {
                super.windowClosing(e);
                System.exit(0);
              }
            });
            frame.pack();
//            frame.setSize(400, 500);
            frame.setVisible(true);

            // JavaFX scene code
//              stage.setScene(scene);
//              stage.show();
          }
        });
  }

  @SuppressWarnings("rawtypes") public RootDomain generateAnalysisOutput1 () {
    String assembly = lines(
        "option DEFAULT_SIZE = 32",
        "mov x, [-1, 42]",
        "cmples LE, x, 0",
        "brc LE, isLE:",
        "mov y, [-3, 4]",
        "assert x = [2, 42]",
        "br exit:",
        "isLE:",
        "mov y, [9, 42]",
        "assert x = [-1, 0]",
        "exit:",
        "halt");

    AnalysisFactory analyzer = new AnalysisFactory().disableDomains("Intervals").enableDomains("IntervalSets");
    Analysis<?> result = analyzer.runAnalysis(assembly);
    RootDomain state = result.getState(CallString.root(), RReilAddr.valueOf(8)).get();
    return state;
  }

  @SuppressWarnings("rawtypes") public RootDomain generateAnalysisOutput2 () {
    // if (?) {
    //   x = 4;
    //   p = malloc(x);
    //   *p = exit_label;
    // } else {
    //   p = NULL;
    // }
    // if (p != NULL) {
    //   x = *p;
    //   goto x;                // without Undef domain the jump target is TOP
    // }
    // exit_label:
    String assembly = lines(
        "option DEFAULT_SIZE = 32",
        "cmpeq Z, 0, rnd",
        "brc Z, else:",
        "mov x, 4",
        "prim (p) = malloc(x)",
        "store p, endif2:",
        "br endif1:",
        "else:",
        "mov p, 0",
        "endif1:",
        "cmpeq Z, 0, p",
        "brc Z, endif2:",
        "load x, p",
        //          "br x",
        "endif2:",
        "halt"
      );
    AnalysisFactory analyzer = new AnalysisFactory().enableDomains("Undef", "SupportSet");

    Analysis<?> result = analyzer.runAnalysis(assembly);
    RootDomain state = result.getState(CallString.root(), RReilAddr.valueOf(7)).get();
    return state;
  }

  @SuppressWarnings("rawtypes") public RootDomain generateAnalysisOutput3 () {
    // int n = rnd(0, 100);
    // for (int i = 0; i < n; i++) {
    //   if (p == NULL)
    //     p = malloc (8);
    //   *p = i;
    //   *(p + 4) = i;
    //  }
    // if (p != NULL) {
    //   x = *p;
    //   y = *(p + 4);
    //   t = exit_label;
    //   t = t + x -y;          // without Undef domain x != y and the jump target is invalid
    //   goto t;
    // }
    // exit_label:
    String assembly = lines(
        "option DEFAULT_SIZE = 32",
        "mov p, 0",
        "mov n, [0, 100]",      // int n = rnd(0, 100);
        "mov i, 0",
        "doloop:",
        "cmpeq Z, i, n",
        "brc Z, endloop:",
        "add i, i, 1",
        "cmpeq Z, p, 0",
        "brc Z, alloc:",
        "br endloop:",
        "alloc:",
        "mov size, 8",
        "prim (p) = malloc(size)",
        "store p, i",
        "add p1, p, 4",
        "store p1, i",
        "endloop:",
        "cmpeq Z, 0, p",
        "brc Z, endif2:",
        "load x, p",
        "add p1, p, 4",
        "load y, p1",
        "mov t, endif2:",
        "add t, t, x",
        "sub t, t, y",
        "br t", // this crashes as unknown target without the Undef domain as x != y and thus t unbounded
        "endif2:",
        "mov u, [-oo, +oo]",
        "mov v, [-oo, +oo]",
        "mov z, [-oo, +oo]",
        "halt"
      );
    AnalysisFactory analyzer =
      new AnalysisFactory().enableDomains("Undef", "SupportSet");
//    DebugHelper.analysisKnobs.enableCommon();
    Analysis<?> result = analyzer.runAnalysis(assembly);
    RootDomain state = result.getState(CallString.root(), RReilAddr.valueOf(0x1a)).get();
    return state;
  }

}
