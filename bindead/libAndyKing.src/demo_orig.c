#include <iostream>
#include <string.h>

#include "simp/SimpSolver.h"
#include "core/SolverTypes.h"
#include "mtl/Vec.h"

using namespace Minisat;

void printClause(int* clause);
bool project(vec<int*>& clauses, int scope, vec<int*>& projects);
bool double_implicants(vec<int*>& clauses, int scope, bool exists, vec<int*>& projects);

int main(int argc, char *argv[]) {
  vec<int*> clauses;
  int clause0[] = { 2, 0+2*2, 1+2*0 }; // -x2 v x0
  int clause1[] = { 2, 0+2*1, 1+2*2 }; // -x1 v x2
  int clause2[] = { 1, 1+2*3 }; // x3
  int clause3[] = { 1, 0+2*3 }; // x3
  clauses.push(clause0);
  clauses.push(clause1);
  clauses.push(clause2);
  //clauses.push(clause3);
  int scope = 2;
  
  for (int i = 0; i < clauses.size(); i++) {
    printClause(clauses[i]);
  }
  
  vec<int*> projects;
  projects.clear();
  double_implicants(clauses, scope, true, projects);

  printf("----\n");
  for (int i = 0; i < projects.size(); i++) {
    printClause(projects[i]);
  }

  vec<int*> projects2;
  projects2.clear();
  double_implicants(projects, scope, false, projects2);
  
  printf("----\n");
  for (int i = 0; i < projects2.size(); i++) {
    printClause(projects2[i]);
  }
}
