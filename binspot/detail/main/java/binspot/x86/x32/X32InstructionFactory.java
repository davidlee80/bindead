package binspot.x86.x32;

import binspot.x86.common.X86DecodeCtx;
import binspot.x86.common.X86Instruction.NativeX32Instruction;
import binspot.x86.common.X86InstructionFactory;
import binspot.x86.common.X86Operand;

/**
 * Factory implementation for X86 32bit instructions with support for mnemonic macros.
 *
 * @author mb0
 */
public class X32InstructionFactory extends X86InstructionFactory {
  @Override public NativeX32Instruction make (String name, X86DecodeCtx ctx) {
    return new NativeX32Instruction(applyMnemonicMacros(name, ctx.getPrefixes()), null, null, null, ctx);
  }

  @Override public NativeX32Instruction make (String name, X86Operand op1, X86DecodeCtx ctx) {
    return new NativeX32Instruction(applyMnemonicMacros(name, ctx.getPrefixes()), op1, null, null, ctx);
  }

  @Override public NativeX32Instruction make (String name, X86Operand op1, X86Operand op2, X86DecodeCtx ctx) {
    return new NativeX32Instruction(applyMnemonicMacros(name, ctx.getPrefixes()), op1, op2, null, ctx);
  }

  @Override public NativeX32Instruction make (String name, X86Operand op1, X86Operand op2, X86Operand op3, X86DecodeCtx ctx) {
    return new NativeX32Instruction(applyMnemonicMacros(name, ctx.getPrefixes()), op1, op2, op3, ctx);
  }
}
